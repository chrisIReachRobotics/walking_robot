%%
%HIP

if (doPlot==true)
figure
 hold on
 plot(FrontLeft_Out.hip_rpm);
 title('Front Left Hip RPM');
legend('raw');
 xlabel('time');
 ylabel('speed RPM');
hold off
end

frontLeftMaxHipRPM = max(FrontLeft_Out.hip_rpm);
frontLeftMinHipRPM = min(FrontLeft_Out.hip_rpm);

%% 
%THIGH

if (doPlot==true)
figure
hold on
plot(FrontLeft_Out.thigh_rpm);
title('Front Left Thigh RPM');
legend('raw');
xlabel('time');
ylabel('speed RPM');
hold off
end

frontLeftMaxThighRPM = max(FrontLeft_Out.thigh_rpm);
frontLeftMinThighRPM = min(FrontLeft_Out.thigh_rpm);

%% 
%KNEE

if (doPlot==true)
figure
hold on
plot(FrontLeft_Out.knee_rpm);
title('Front Left Knee RPM');
legend('raw');
xlabel('time');
ylabel('speed RPM');
hold off
end

frontLeftMaxKneeRPM = max(FrontLeft_Out.knee_rpm);
frontLeftMinKneeRPM = min(FrontLeft_Out.knee_rpm);
