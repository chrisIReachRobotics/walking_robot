if (doPlot==true)
figure
 hold on
 plot(FrontLeft_Out.Normal_Force);
 title('Front Left GRF');
legend('raw');
 xlabel('time');
 ylabel('GRF N');
hold off
end

frontLeftMaxHipRPM = max(FrontLeft_Out.hip_rpm);