interval = [1 10]; %Filtering Interval - 1st digit influences the amplitude of the signal, second influences the frequency
doPlot = true;

%%
%Front Left
frontLeftTotalForceMean=mean(FrontLeft_Out.hip_force);
frontLeftFilteredTotalForce=idealfilter(FrontLeft_Out.hip_force,interval,'pass');
frontLeftFilteredTotalForce=frontLeftFilteredTotalForce+frontLeftTotalForceMean;

if (doPlot==true)
figure
 hold on
%  plot(FrontLeft_Out.hip_force);
 plot(FrontLeft_Out.hip_force);
 title('Front Left Hip Force');
legend('x','y','z');
 xlabel('time');
 ylabel('F N');
hold off
print('FrontLeftHipForce','-dpng');
end

%%
%Front Right
frontRightTotalForceMean=mean(FrontRight_Out.hip_force);
frontRightFilteredTotalForce=idealfilter(FrontRight_Out.hip_force,interval,'pass');
frontRightFilteredTotalForce=frontRightFilteredTotalForce+frontRightTotalForceMean;

if (doPlot==true)
figure
 hold on
%  plot(FrontRight_Out.hip_force);
 plot(frontRightFilteredTotalForce);
 title('Front Right Hip Force');
legend('x','y','z');
 xlabel('time');
 ylabel('F N');
hold off
print('FrontRightHipForce','-dpng');
end

%%
%Back Left
backLeftTotalForceMean=mean(BackLeft_Out.hip_force);
backLeftFilteredTotalForce=idealfilter(BackLeft_Out.hip_force,interval,'pass');
backLeftFilteredTotalForce=backLeftFilteredTotalForce+backLeftTotalForceMean;

if (doPlot==true)
figure
 hold on
%  plot(FrontLeft_Out.hip_force);
 plot(backLeftFilteredTotalForce);
 title('Back Left Hip Force');
legend('x','y','z');
 xlabel('time');
 ylabel('F N');
hold off
print('BackLeftHipForce','-dpng');
end

%%
%Back Right
backRightTotalForceMean=mean(BackRight_Out.hip_force);
backRightFilteredTotalForce=idealfilter(BackRight_Out.hip_force,interval,'pass');
backRightFilteredTotalForce=backRightFilteredTotalForce+backRightTotalForceMean;

if (doPlot==true)
figure
 hold on
%  plot(FrontRight_Out.hip_force);
 plot(backRightFilteredTotalForce);
 title('Back Right Hip Force');
legend('x','y','z');
 xlabel('time');
 ylabel('F N');
hold off
print('BackRightHipForce','-dpng');
end
