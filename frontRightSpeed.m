%%
%HIP

if (doPlot==true)
figure
 hold on
 plot(FrontRight_Out.hip_rpm);
 title('Front Right Hip RPM');
legend('raw');
 xlabel('time');
 ylabel('speed RPM');
hold off
end

frontRightMaxHipRPM = max(FrontRight_Out.hip_rpm);
frontRightMinHipRPM = min(FrontRight_Out.hip_rpm);

%% 
%THIGH

if (doPlot==true)
figure
hold on
plot(FrontRight_Out.thigh_rpm);
title('Front Right Thigh RPM');
legend('raw');
xlabel('time');
ylabel('speed RPM');
hold off
end

frontRightMaxThighRPM = max(FrontRight_Out.thigh_rpm);
frontRightMinThighRPM = min(FrontRight_Out.thigh_rpm);

%% 
%KNEE

if (doPlot==true)
figure
hold on
plot(FrontRight_Out.knee_rpm);
title('Front Right Knee RPM');
legend('raw');
xlabel('time');
ylabel('speed RPM');
hold off
end

frontRightMaxKneeRPM = max(FrontRight_Out.knee_rpm);
frontRightMinKneeRPM = min(FrontRight_Out.knee_rpm);
