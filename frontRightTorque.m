%%
%HIP
frontRightRawHipMean=mean(FrontRight_Out.hip_torque);
frontRightFilteredHip=idealfilter(FrontRight_Out.hip_torque,interval,'pass');
frontRightFilteredHip=frontRightFilteredHip+frontRightRawHipMean;

if (doPlot==true)
figure
 hold on
 plot(FrontRight_Out.hip_torque);
 plot(frontRightFilteredHip);
 title('Front Right Hip');
legend('raw','filtered');
 xlabel('time');
 ylabel('torque Nm');
hold off
end

frontRightMaxHip = max(frontRightFilteredHip);
frontRightMinHip = min(frontRightFilteredHip);

%% 
%THIGH
frontRightRawThighMean=mean(FrontRight_Out.thigh_torque);
frontRightFilteredThigh=idealfilter(FrontRight_Out.thigh_torque,interval,'pass');
frontRightFilteredThigh=frontRightFilteredThigh+frontRightRawThighMean;

if (doPlot==true)
figure
hold on
plot(FrontRight_Out.thigh_torque);
plot(frontRightFilteredThigh);
title('Front Right Thigh');
legend('raw','filtered');
xlabel('time');
ylabel('torque Nm');
hold off
end

frontRightMaxThigh = max(frontRightFilteredThigh);
frontRightMinThigh = min(frontRightFilteredThigh);

%% 
%KNEE
frontRightRawKneeMean=mean(FrontRight_Out.knee_torque);
frontRightFilteredKnee=idealfilter(FrontRight_Out.knee_torque,interval,'pass');
frontRightFilteredKnee=frontRightFilteredKnee+frontRightRawKneeMean;

if (doPlot==true)
figure
hold on
plot(FrontRight_Out.knee_torque);
plot(frontRightFilteredKnee);
title('Front Right Knee');
legend('raw','filtered');
xlabel('time');
ylabel('torque Nm');
hold off
end

frontRightMaxKnee = max(frontRightFilteredKnee);
frontRightMinKnee = min(frontRightFilteredKnee);