%%
%HIP

if (doPlot==true)
figure
 hold on
 plot(BackLeft_Out.hip_rpm);
 title('Back Left Hip RPM');
legend('raw');
 xlabel('time');
 ylabel('speed RPM');
hold off
end

backLeftMaxHipRPM = max(BackLeft_Out.hip_rpm);
backLeftMinHipRPM = min(BackLeft_Out.hip_rpm);

%% 
%THIGH

if (doPlot==true)
figure
hold on
plot(BackLeft_Out.thigh_rpm);
title('Back Left Thigh RPM');
legend('raw');
xlabel('time');
ylabel('speed RPM');
hold off
end

backLeftMaxThighRPM = max(BackLeft_Out.thigh_rpm);
backLeftMinThighRPM = min(BackLeft_Out.thigh_rpm);

%% 
%KNEE

if (doPlot==true)
figure
hold on
plot(BackLeft_Out.knee_rpm);
title('Back Left Knee RPM');
legend('raw');
xlabel('time');
ylabel('speed RPM');
hold off
end

backLeftMaxKneeRPM = max(BackLeft_Out.knee_rpm);
backLeftMinKneeRPM = min(BackLeft_Out.knee_rpm);
