disp('Start New Loading Variables');

%JOINT SIZES
hip_length = 28.1;
thigh_length = 88;
shin_length = 105.5;

shin_width = 0.01;
shin_depth = 0.01;
hip_to_hip = 98;
mainBody_length = hip_to_hip;
mainBody_width = hip_to_hip;
mainBody_height = 50;

%Beserker mass
% hip_mass = 0.007; %7g
% thigh_mass = 0.065; %65g
% shin_mass = 0.059; %shin 31g, shield 28g
% mainBody_mass = 0.52; %battery and mainbody = 344g, 4 x hips = 40g each, 2 weapons

%Brute mass
hip_mass = 0.015; %7g
thigh_mass = 0.114; %2 x B07 gearboxes (31.9g), plus 50g (measured mass of form 2 printed shells at 35g). 
shin_mass = 0.059; %Copied from beserker: no reason to suspect this will be any heavier from beserker.
mainBody_mass = 0.614; %battery and mainbody = 344g, 4 x hips = 55g each, 2 weapons+50g

%WORLD VARIABLES
contact_radius = 0.01;
height_plane = 0.025;
plane_x = 25;
plane_y  = 25;
world_damping = 0.25;
world_rot_damping = 0.25;
motion_time_constant = 0.015;
init_height = 0.10;
contact_point_radius = 1e-4;

%JOINT RANGE (DEGREES)
knee_deg_out = 140; %157
knee_deg_in = 0; %17 add 17
thigh_deg_down=165; %175
thigh_deg_up=0; %10 add 10
hip_deg_right=60; %150
hip_deg_left=-60; %30

knee_deg_mid = 73; %157
thigh_deg_mid=80; %175
hip_deg_mid=0; %150

knee_deg_to_make_shin_horizontal_when_thigh_at_0 = 83;

%JOINT RANGE (ADC TICKS)
hip_right = 385; %385
hip_left = 833; %833
thigh_down = 160; %160
thigh_up = 656; %656
knee_out=402; %402
knee_in=840; %840

%LEG DATA IMPORT
rawData=csvread('Book1.csv');

%rawData=LegPositionstwo;
%rawData(all(~rawData,2),:)=[]; %remove empty rows.
sz=size(rawData);
rows = sz(1,1);

time = zeros(rows,1);
frontLeftHip=zeros(rows,1);
frontLeftKnee=zeros(rows,1);
frontLeftThigh=zeros(rows,1);
backLeftHip=zeros(rows,1);
backLeftKnee=zeros(rows,1);
backLeftThigh=zeros(rows,1);
frontRightHip=zeros(rows,1);
frontRightKnee=zeros(rows,1);
frontRightThigh=zeros(rows,1);
backRightHip=zeros(rows,1);
backRightKnee=zeros(rows,1);
backRightThigh=zeros(rows,1);

localtime=0;
freeze_frame = 300;

%CONVERT DATA FROM ADC TICKS TO DEGREES
for idx = 1:rows
    time(idx) = localtime;
    localtime = localtime+0.02;
    
%     if idx<300
%         if (idx>100)&&(idx<=150)  
%              frontRightHip(idx,1)=hip_deg_left;
%     frontRightThigh(idx,1)=thigh_deg_up;
%     frontRightKnee(idx,1)=knee_deg_to_make_shin_horizontal_when_thigh_at_0;
%     
%     backRightHip(idx,1)=hip_deg_left;
%     backRightThigh(idx,1)=thigh_deg_up;
%     backRightKnee(idx,1)=knee_deg_to_make_shin_horizontal_when_thigh_at_0;
%     
%     backLeftHip(idx,1)=hip_deg_left;
%     backLeftThigh(idx,1)=thigh_deg_up;
%     backLeftKnee(idx,1)=knee_deg_to_make_shin_horizontal_when_thigh_at_0;
%     
%     frontLeftHip(idx,1)=hip_deg_left;
%     frontLeftThigh(idx,1)=thigh_deg_up;
%     frontLeftKnee(idx,1)=knee_deg_to_make_shin_horizontal_when_thigh_at_0;
%         elseif (idx>150)&&(idx<=200)
%                  frontRightHip(idx,1)=hip_deg_mid;
%     frontRightThigh(idx,1)=thigh_deg_mid;
%     frontRightKnee(idx,1)=knee_deg_mid;
%     
%     backRightHip(idx,1)=hip_deg_mid;
%     backRightThigh(idx,1)=thigh_deg_mid;
%     backRightKnee(idx,1)=knee_deg_mid;
%     
%     backLeftHip(idx,1)=hip_deg_mid;
%     backLeftThigh(idx,1)=thigh_deg_mid;
%     backLeftKnee(idx,1)=knee_deg_mid;
%     
%     frontLeftHip(idx,1)=hip_deg_mid;
%     frontLeftThigh(idx,1)=thigh_deg_mid;
%     frontLeftKnee(idx,1)=knee_deg_mid;
%         elseif idx>200
%     frontRightHip(idx,1)=hip_deg_right;
%     frontRightThigh(idx,1)=thigh_deg_down;
%     frontRightKnee(idx,1)=knee_deg_out;
%     
%     backRightHip(idx,1)=hip_deg_right;
%     backRightThigh(idx,1)=thigh_deg_down;
%     backRightKnee(idx,1)=knee_deg_out;
%     
%     backLeftHip(idx,1)=hip_deg_right;
%     backLeftThigh(idx,1)=thigh_deg_down;
%     backLeftKnee(idx,1)=knee_deg_out;
%     
%     frontLeftHip(idx,1)=hip_deg_right;
%     frontLeftThigh(idx,1)=thigh_deg_down;
%     frontLeftKnee(idx,1)=knee_deg_out;
%         end
% else 
%          element = rawData(300,1);
%     frontRightHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(300,2);
%     frontRightThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(300,3);
%     frontRightKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     
%     element = rawData(300,4);
%     backRightHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(300,5);
%     backRightThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(300,6);
%     backRightKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     
%     element = rawData(300,7);
%     backLeftHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(300,8);
%     backLeftThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(300,9);
%     backLeftKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     
%     
%     element = rawData(300,10);
%     frontLeftHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(300,11);
%     frontLeftThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(300,12);
%     frontLeftKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     end


%%%%%%% REAL WALKING
    element = rawData(idx,1);
    frontRightHip(idx,1)=((element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left))+hip_deg_left;
    element = rawData(idx,2);
    frontRightThigh(idx,1)=((element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up))+thigh_deg_up;
    element = rawData(idx,3);
    frontRightKnee(idx,1)=((element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in))+knee_deg_in;
    
    element = rawData(idx,4);
    backRightHip(idx,1)=((element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left))+hip_deg_left;
    element = rawData(idx,5);
    backRightThigh(idx,1)=((element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up))+thigh_deg_up;
    element = rawData(idx,6);
    backRightKnee(idx,1)=((element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in))+knee_deg_in;
    
    element = rawData(idx,7);
    backLeftHip(idx,1)=((element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left))+hip_deg_left;
    element = rawData(idx,8);
    backLeftThigh(idx,1)=((element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up))+thigh_deg_up;
    element = rawData(idx,9);
    backLeftKnee(idx,1)=((element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in))+knee_deg_in;
    
    
    element = rawData(idx,10);
    frontLeftHip(idx,1)=((element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left))+hip_deg_left;
    element = rawData(idx,11);
    frontLeftThigh(idx,1)=((element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up))+thigh_deg_up;
    element = rawData(idx,12);
    frontLeftKnee(idx,1)=((element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in))+knee_deg_in;


    %%%% FREEZE FRAME CODE
%     if idx < freeze_frame
%     element = rawData(idx,1);
%     frontRightHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(idx,2);
%     frontRightThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(idx,3);
%     frontRightKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     
%     element = rawData(idx,4);
%     backRightHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(idx,5);
%     backRightThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(idx,6);
%     backRightKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     
%     element = rawData(idx,7);
%     backLeftHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(idx,8);
%     backLeftThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(idx,9);
%     backLeftKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     
%     
%     element = rawData(idx,10);
%     frontLeftHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(idx,11);
%     frontLeftThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(idx,12);
%     frontLeftKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     else
%         element = rawData(freeze_frame,1);
%     frontRightHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(freeze_frame,2);
%     frontRightThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(freeze_frame,3);
%     frontRightKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     
%     element = rawData(freeze_frame,4);
%     backRightHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(freeze_frame,5);
%     backRightThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(freeze_frame,6);
%     backRightKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     
%     element = rawData(freeze_frame,7);
%     backLeftHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(freeze_frame,8);
%     backLeftThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(freeze_frame,9);
%     backLeftKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     
%     
%     element = rawData(freeze_frame,10);
%     frontLeftHip(idx,1)=(element-hip_left)*(hip_deg_right-hip_deg_left)/(hip_right-hip_left)+hip_deg_left;
%     element = rawData(freeze_frame,11);
%     frontLeftThigh(idx,1)=(element-thigh_up)*(thigh_deg_down-thigh_deg_up)/(thigh_down-thigh_up)+thigh_deg_up;
%     element = rawData(freeze_frame,12);
%     frontLeftKnee(idx,1)=(element-knee_in)*(knee_deg_out-knee_deg_in)/(knee_out-knee_in)+knee_deg_in;
%     end
end


%xx = 0:.1:28.86;
%frontLeftHipI = spline(time, frontLeftHip,xx);
% frontLeftHipI = interp1(time,frontLeftHip, 0:2:max(time), 'spline');
%plot(time, frontLeftHip, xx, frontLeftHipI),legend('Raw', 'Interpolated');
%plot(time,frontLeftHipI)


disp('End Loading Variables');