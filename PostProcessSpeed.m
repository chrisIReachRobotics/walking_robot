doPlot = true;

%%
frontLeftSpeed;
frontRightSpeed;
backLeftSpeed;
backRightSpeed;

%%
% fprintf('******\n');
% fprintf('******\n');
% fprintf('Front Left Hip Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',frontLeftMaxHipRPM, frontLeftMinHipRPM);
% fprintf('Front Right Hip Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',frontRightMaxHipRPM, frontRightMinHipRPM);
% fprintf('Back Left Hip Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',backLeftMaxHipRPM, backLeftMinHipRPM);
% fprintf('Back Right Hip Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',backRightMaxHipRPM, backRightMinHipRPM);
% fprintf('******\n');
% fprintf('Front Left Thigh Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',frontLeftMaxThighRPM, frontLeftMinThighRPM);
% fprintf('Front Right Thigh Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',frontRightMaxThighRPM, frontRightMinThighRPM);
% fprintf('Back Left Thigh Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',backLeftMaxThighRPM, backLeftMinThighRPM);
% fprintf('Back Right Thigh Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',backRightMaxThighRPM, backRightMinThighRPM);
% fprintf('******\n');
% fprintf('Front Left Knee Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',frontLeftMaxKneeRPM, frontLeftMinKneeRPM);
% fprintf('Front Right Knee Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',frontRightMaxKneeRPM, frontRightMinKneeRPM);
% fprintf('Back Left Knee Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',backLeftMaxKneeRPM, backLeftMinKneeRPM);
% fprintf('Back Right Knee Max Speed: %2.2f RPM Min Speed: %2.2f RPM\n',backRightMaxKneeRPM, backRightMinKneeRPM);

%%
%CALCULATE ABSOLUTE MAX RPM
%Front Left
FrontLeftHipMaxRPM = max([abs(frontLeftMaxHipRPM),abs(frontLeftMinHipRPM)]);
FrontLeftThighMaxRPM = max([abs(frontLeftMaxThighRPM),abs(frontLeftMinThighRPM)]);
FrontLeftKneeMaxRPM = max([abs(frontLeftMaxKneeRPM),abs(frontLeftMinKneeRPM)]);
%Back Left
BackLeftHipMaxRPM = max([abs(backLeftMaxHipRPM),abs(backLeftMinHipRPM)]);
BackLeftThighMaxRPM = max([abs(backLeftMaxThighRPM),abs(backLeftMinThighRPM)]);
BackLeftKneeMaxRPM = max([abs(backLeftMaxKneeRPM),abs(backLeftMinKneeRPM)]);
%Front Right
FrontRightHipMaxRPM = max([abs(frontRightMaxHipRPM),abs(frontRightMinHipRPM)]);
FrontRightThighMaxRPM = max([abs(frontRightMaxThighRPM),abs(frontRightMinThighRPM)]);
FrontRightKneeMaxRPM = max([abs(frontRightMaxKneeRPM),abs(frontRightMinKneeRPM)]);
%Back Right
BackRightHipMaxRPM = max([abs(backRightMaxHipRPM),abs(backRightMinHipRPM)]);
BackRightThighMaxRPM = max([abs(backRightMaxThighRPM),abs(backRightMinThighRPM)]);
BackRightKneeMaxRPM = max([abs(backRightMaxKneeRPM),abs(backRightMinKneeRPM)]);

%%
%Display Max RPM per Leg
fprintf('******\n');
fprintf('******\n');
fprintf('Front Left Hip Max Speed: %2.2f\n',FrontLeftHipMaxRPM);
fprintf('Front Right Hip Max Speed: %2.2f\n',FrontRightHipMaxRPM);
fprintf('Back Left Hip Max Speed: %2.2f\n',BackLeftHipMaxRPM);
fprintf('Back Right Hip Max Speed: %2.2f\n',BackRightHipMaxRPM);
fprintf('******\n');
fprintf('Front Left Thigh Max Speed: %2.2f\n',FrontLeftThighMaxRPM);
fprintf('Front Right Thigh Max Speed: %2.2f\n',FrontRightThighMaxRPM);
fprintf('Back Left Thigh Max Speed: %2.2f\n',BackLeftThighMaxRPM);
fprintf('Back Right Thigh Max Speed: %2.2f\n',BackRightThighMaxRPM);
fprintf('******\n');
fprintf('Front Left Knee Max Speed: %2.2f\n',FrontLeftKneeMaxRPM);
fprintf('Front Right Knee Max Speed: %2.2f\n',FrontRightKneeMaxRPM);
fprintf('Back Left Knee Max Speed: %2.2f\n',BackLeftKneeMaxRPM);
fprintf('Back Right Knee Max Speed: %2.2f\n',BackRightKneeMaxRPM);
fprintf('******\n');

%%
%Summary values
%Ignore Back Right
% hipMaxRPM = max([FrontLeftHipMaxRPM, FrontRightHipMaxRPM, BackLeftHipMaxRPM, BackRightHipMaxRPM]);
% thighMaxRPM = max([FrontLeftThighMaxRPM, FrontRightThighMaxRPM, BackLeftThighMaxRPM, BackRightThighMaxRPM]);
% kneeMaxRPM = max([FrontLeftKneeMaxRPM,FrontRightKneeMaxRPM,BackLeftKneeMaxRPM, BackRightKneeMaxRPM]);

% %Ignore Back Right
 hipMaxRPM = max([FrontLeftHipMaxRPM, FrontRightHipMaxRPM, BackLeftHipMaxRPM]);
 thighMaxRPM = max([FrontLeftThighMaxRPM, FrontRightThighMaxRPM, BackLeftThighMaxRPM]);
 kneeMaxRPM = max([FrontLeftKneeMaxRPM,FrontRightKneeMaxRPM,BackLeftKneeMaxRPM]);

fprintf('******\n');
fprintf('Absolute Max Hip: %2.2f RPM, Thigh: %2.2f RPM, Knee: %2.2f RPM\n', hipMaxRPM, thighMaxRPM, kneeMaxRPM);
fprintf('******\n');