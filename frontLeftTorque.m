%%
%HIP
frontLeftRawHipMean=mean(FrontLeft_Out.hip_torque);
frontLeftFilteredHip=idealfilter(FrontLeft_Out.hip_torque,interval,'pass');
frontLeftFilteredHip=frontLeftFilteredHip+frontLeftRawHipMean;

if (doPlot==true)
figure
 hold on
 plot(FrontLeft_Out.hip_torque);
 plot(frontLeftFilteredHip);
 title('Front Left Hip');
legend('raw','filtered');
 xlabel('time');
 ylabel('torque Nm');
hold off
end

frontLeftMaxHip = max(frontLeftFilteredHip);
frontLeftMinHip = min(frontLeftFilteredHip);

%% 
%THIGH
frontLeftRawThighMean=mean(FrontLeft_Out.thigh_torque);
frontLeftFilteredThigh=idealfilter(FrontLeft_Out.thigh_torque,interval,'pass');
frontLeftFilteredThigh=frontLeftFilteredThigh+frontLeftRawThighMean;

if (doPlot==true)
figure
hold on
plot(FrontLeft_Out.thigh_torque);
plot(frontLeftFilteredThigh);
title('Front Left Thigh');
legend('raw','filtered');
xlabel('time');
ylabel('torque Nm');
hold off
end

frontLeftMaxThigh = max(frontLeftFilteredThigh);
frontLeftMinThigh = min(frontLeftFilteredThigh);

%% 
%KNEE
frontLeftRawKneeMean=mean(FrontLeft_Out.knee_torque);
frontLeftFilteredKnee=idealfilter(FrontLeft_Out.knee_torque,interval,'pass');
frontLeftFilteredKnee=frontLeftFilteredKnee+frontLeftRawKneeMean;

if (doPlot==true)
figure
hold on
plot(FrontLeft_Out.knee_torque);
plot(frontLeftFilteredKnee);
title('Front Left Knee');
legend('raw','filtered');
xlabel('time');
ylabel('torque Nm');
hold off
end

frontLeftMaxKnee = max(frontLeftFilteredKnee);
frontLeftMinKnee = min(frontLeftFilteredKnee);
