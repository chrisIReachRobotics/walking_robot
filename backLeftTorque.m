%%
%HIP
backLeftRawHipMean=mean(BackLeft_Out.hip_torque);
backLeftFilteredHip=idealfilter(BackLeft_Out.hip_torque,interval,'pass');
backLeftFilteredHip=backLeftFilteredHip+backLeftRawHipMean;

if (doPlot==true)
figure
 hold on
 plot(BackLeft_Out.hip_torque);
 plot(backLeftFilteredHip);
 title('Back Left Hip');
legend('raw','filtered');
 xlabel('time');
 ylabel('torque Nm');
hold off
end

backLeftMaxHip = max(backLeftFilteredHip);
backLeftMinHip = min(backLeftFilteredHip);

%% 
%THIGH
backLeftRawThighMean=mean(BackLeft_Out.thigh_torque);
backLeftFilteredThigh=idealfilter(BackLeft_Out.thigh_torque,interval,'pass');
backLeftFilteredThigh=backLeftFilteredThigh+backLeftRawThighMean;

if (doPlot==true)
figure
hold on
plot(BackLeft_Out.thigh_torque);
plot(backLeftFilteredThigh);
title('Back Left Thigh');
legend('raw','filtered');
xlabel('time');
ylabel('torque Nm');
hold off
end

backLeftMaxThigh = max(backLeftFilteredThigh);
backLeftMinThigh = min(backLeftFilteredThigh);

%% 
%KNEE
backLeftRawKneeMean=mean(BackLeft_Out.knee_torque);
backLeftFilteredKnee=idealfilter(BackLeft_Out.knee_torque,interval,'pass');
backLeftFilteredKnee=backLeftFilteredKnee+backLeftRawKneeMean;

if (doPlot==true)
figure
hold on
plot(BackLeft_Out.knee_torque);
plot(backLeftFilteredKnee);
title('Back Left Knee');
legend('raw','filtered');
xlabel('time');
ylabel('torque Nm');
hold off
end

backLeftMaxKnee = max(backLeftFilteredKnee);
backLeftMinKnee = min(backLeftFilteredKnee);
