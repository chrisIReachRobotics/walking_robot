%%
%HIP

if (doPlot==true)
figure
 hold on
 plot(BackRight_Out.hip_rpm);
 title('Back Right Hip RPM');
legend('raw');
 xlabel('time');
 ylabel('speed RPM');
hold off
end

backRightMaxHipRPM = max(BackRight_Out.hip_rpm);
backRightMinHipRPM = min(BackRight_Out.hip_rpm);

%% 
%THIGH

if (doPlot==true)
figure
hold on
plot(BackRight_Out.thigh_rpm);
title('Back Right Thigh RPM');
legend('raw');
xlabel('time');
ylabel('speed RPM');
hold off
end

backRightMaxThighRPM = max(BackRight_Out.thigh_rpm);
backRightMinThighRPM = min(BackRight_Out.thigh_rpm);

%% 
%KNEE

if (doPlot==true)
figure
hold on
plot(BackRight_Out.knee_rpm);
title('Back Right Knee RPM');
legend('raw');
xlabel('time');
ylabel('speed RPM');
hold off
end

backRightMaxKneeRPM = max(BackRight_Out.knee_rpm);
backRightMinKneeRPM = min(BackRight_Out.knee_rpm);