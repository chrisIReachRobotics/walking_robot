interval = [1 10]; %Filtering Interval - 1st digit influences the amplitude of the signal, second influences the frequency
doPlot = true;

%%
%Front Left
frontLeftGRFMean=mean(FrontLeft_Out.Normal_Force);
frontLeftFilteredGRF=idealfilter(FrontLeft_Out.Normal_Force,interval,'pass');
frontLeftFilteredGRF=frontLeftFilteredGRF+frontLeftGRFMean;

if (doPlot==true)
figure
 hold on
 plot(FrontLeft_Out.Normal_Force);
 plot(frontLeftFilteredGRF);
 title('Front Left GRF');
legend('raw','filtered');
 xlabel('time');
 ylabel('GRF N');
hold off
print('FrontLeftGRF','-dpng');
end

% frontLeftMaxGRF = max(FrontLeft_Out.Normal_Force);
frontLeftMaxGRF = max(frontLeftFilteredGRF);

%%
%Front Right
frontRightGRFMean=mean(FrontRight_Out.Normal_Force);
frontRightFilteredGRF=idealfilter(FrontRight_Out.Normal_Force,interval,'pass');
frontRightFilteredGRF=frontRightFilteredGRF+frontRightGRFMean;

if (doPlot==true)
figure
 hold on
 plot(FrontRight_Out.Normal_Force);
 plot(frontRightFilteredGRF);
 title('Front Right GRF');
legend('raw','filtered');
 xlabel('time');
 ylabel('GRF N');
hold off
print('FrontRightGRF','-dpng');
end

% frontRightMaxGRF = max(FrontRight_Out.Normal_Force);
frontRightMaxGRF = max(frontRightFilteredGRF);

%%
%Back Left
backLeftGRFMean=mean(BackLeft_Out.Normal_Force);
backLeftFilteredGRF=idealfilter(BackLeft_Out.Normal_Force,interval,'pass');
backLeftFilteredGRF=backLeftFilteredGRF+backLeftGRFMean;

if (doPlot==true)
figure
 hold on
 plot(BackLeft_Out.Normal_Force);
 plot(backLeftFilteredGRF);
 title('Back Left GRF');
legend('raw','filtered');
 xlabel('time');
 ylabel('GRF N');
hold off
print('BackLeftGRF','-dpng');
end

% backLeftMaxGRF = max(BackLeft_Out.Normal_Force);
backLeftMaxGRF = max(backLeftFilteredGRF);

%%
%Back Right
backRightGRFMean=mean(BackRight_Out.Normal_Force);
backRightFilteredGRF=idealfilter(BackRight_Out.Normal_Force,interval,'pass');
backRightFilteredGRF=backRightFilteredGRF+backRightGRFMean;

if (doPlot==true)
figure
 hold on
 plot(BackRight_Out.Normal_Force);
 plot(backRightFilteredGRF);
 title('Back Right GRF');
legend('raw','filtered');
 xlabel('time');
 ylabel('GRF N');
hold off
print('BackLeftGRF','-dpng');
end

% backRightMaxGRF = max(BackRight_Out.Normal_Force);
backRightMaxGRF = max(backRightFilteredGRF);

%%
%Display Max GRF per Leg
fprintf('******\n');
fprintf('******\n');
fprintf('Front Left Max GRF: %2.2f\n',frontLeftMaxGRF);
fprintf('Front Right Max GRF: %2.2f\n',frontRightMaxGRF);
fprintf('Back Left Max GRF: %2.2f\n',backLeftMaxGRF);
fprintf('Back Right Max GRF: %2.2f\n',backRightMaxGRF);
fprintf('******\n');

%%
%Summary values

MaxGRF = max([frontLeftMaxGRF, frontRightMaxGRF, backLeftMaxGRF, backRightMaxGRF]);

fprintf('******\n');
fprintf('Absolute Max GRF: %2.2f N\n', MaxGRF);
fprintf('******\n');