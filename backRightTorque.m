%%
%HIP
backRightRawHipMean=mean(BackRight_Out.hip_torque);
backRightFilteredHip=idealfilter(BackRight_Out.hip_torque,interval,'pass');
backRightFilteredHip=backRightFilteredHip+backRightRawHipMean;

if (doPlot==true)
figure
 hold on
 plot(BackRight_Out.hip_torque);
 plot(backRightFilteredHip);
 title('Back Right Hip');
legend('raw','filtered');
 xlabel('time');
 ylabel('torque Nm');
hold off
end

backRightMaxHip = max(backRightFilteredHip);
backRightMinHip = min(backRightFilteredHip);

%% 
%THIGH
backRightRawThighMean=mean(BackRight_Out.thigh_torque);
backRightFilteredThigh=idealfilter(BackRight_Out.thigh_torque,interval,'pass');
backRightFilteredThigh=backRightFilteredThigh+backRightRawThighMean;

if (doPlot==true)
figure
hold on
plot(BackRight_Out.thigh_torque);
plot(backRightFilteredThigh);
title('Back Right Thigh');
legend('raw','filtered');
xlabel('time');
ylabel('torque Nm');
hold off
end

backRightMaxThigh = max(backRightFilteredThigh);
backRightMinThigh = min(backRightFilteredThigh);

%% 
%KNEE
backRightRawKneeMean=mean(BackRight_Out.knee_torque);
backRightFilteredKnee=idealfilter(BackRight_Out.knee_torque,interval,'pass');
backRightFilteredKnee=backRightFilteredKnee+backRightRawKneeMean;

if (doPlot==true)
figure
hold on
plot(BackRight_Out.knee_torque);
plot(backRightFilteredKnee);
title('Back Right Knee');
legend('raw','filtered');
xlabel('time');
ylabel('torque Nm');
hold off
end

backRightMaxKnee = max(backRightFilteredKnee);
backRightMinKnee = min(backRightFilteredKnee);
