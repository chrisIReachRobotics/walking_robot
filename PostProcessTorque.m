interval = [1 10]; %Filtering Interval - 1st digit influences the amplitude of the signal, second influences the frequency
doPlot=false;

%%
frontLeftTorque;
frontRightTorque;
backLeftTorque;
backRightTorque;

%%
% fprintf('******\n');
% fprintf('******\n');
% fprintf('Front Left Hip Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',frontLeftMaxHip, frontLeftMinHip);
% fprintf('Front Right Hip Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',frontRightMaxHip, frontRightMinHip);
% fprintf('Back Left Hip Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',backLeftMaxHip, backLeftMinHip);
% fprintf('Back Right Hip Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',backRightMaxHip, backRightMinHip);
% fprintf('******\n');
% fprintf('Front Left Thigh Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',frontLeftMaxThigh, frontLeftMinThigh);
% fprintf('Front Right Thigh Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',frontRightMaxThigh, frontRightMinThigh);
% fprintf('Back Left Thigh Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',backLeftMaxThigh, backLeftMinThigh);
% fprintf('Back Right Thigh Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',backRightMaxThigh, backRightMinThigh);
% fprintf('******\n');
% fprintf('Front Left Knee Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',frontLeftMaxKnee, frontLeftMinKnee);
% fprintf('Front Right Knee Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',frontRightMaxKnee, frontRightMinKnee);
% fprintf('Back Left Knee Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',backLeftMaxKnee, backLeftMinKnee);
% fprintf('Back Right Knee Max Torque: %2.2f Nm Min Torque: %2.2f Nm\n',backRightMaxKnee, backRightMinKnee);

%%
%CALCULATE ABSOLUTE MAX NM
%Front Left
FrontLeftHipMaxTorque = max([abs(frontLeftMaxHip),abs(frontLeftMinHip)]);
FrontLeftThighMaxTorque = max([abs(frontLeftMaxThigh),abs(frontLeftMinThigh)]);
FrontLeftKneeMaxTorque = max([abs(frontLeftMaxKnee),abs(frontLeftMinKnee)]);
%Back Left
BackLeftHipMaxTorque = max([abs(backLeftMaxHip),abs(backLeftMinHip)]);
BackLeftThighMaxTorque = max([abs(backLeftMaxThigh),abs(backLeftMinThigh)]);
BackLeftKneeMaxTorque = max([abs(backLeftMaxKnee),abs(backLeftMinKnee)]);
%Front Right
FrontRightHipMaxTorque = max([abs(frontRightMaxHip),abs(frontRightMinHip)]);
FrontRightThighMaxTorque = max([abs(frontRightMaxThigh),abs(frontRightMinThigh)]);
FrontRightKneeMaxTorque = max([abs(frontRightMaxKnee),abs(frontRightMinKnee)]);
%Back Right
BackRightHipMaxTorque = max([abs(backRightMaxHip),abs(backRightMinHip)]);
BackRightThighMaxTorque = max([abs(backRightMaxThigh),abs(backRightMinThigh)]);
BackRightKneeMaxTorque = max([abs(backRightMaxKnee),abs(backRightMinKnee)]);

%%
%Display Max Torque per Leg
fprintf('******\n');
fprintf('******\n');
fprintf('Front Left Hip Max Torque: %2.2f\n',FrontLeftHipMaxTorque);
fprintf('Front Right Hip Max Torque: %2.2f\n',FrontRightHipMaxTorque);
fprintf('Back Left Hip Max Torque: %2.2f\n',BackLeftHipMaxTorque);
fprintf('Back Right Hip Max Torque: %2.2f\n',BackRightHipMaxTorque);
fprintf('******\n');
fprintf('Front Left Thigh Max Torque: %2.2f\n',FrontLeftThighMaxTorque);
fprintf('Front Right Thigh Max Torque: %2.2f\n',FrontRightThighMaxTorque);
fprintf('Back Left Thigh Max Torque: %2.2f\n',BackLeftThighMaxTorque);
fprintf('Back Right Thigh Max Torque: %2.2f\n',BackRightThighMaxTorque);
fprintf('******\n');
fprintf('Front Left Knee Max Torque: %2.2f\n',FrontLeftKneeMaxTorque);
fprintf('Front Right Knee Max Torque: %2.2f\n',FrontRightKneeMaxTorque);
fprintf('Back Left Knee Max Torque: %2.2f\n',BackLeftKneeMaxTorque);
fprintf('Back Right Knee Max Torque: %2.2f\n',BackRightKneeMaxTorque);
fprintf('******\n');

%%
%Summary values
hipMax = max([FrontLeftHipMaxTorque,FrontRightHipMaxTorque,BackLeftHipMaxTorque,BackRightHipMaxTorque]);
thighMax = max([FrontLeftThighMaxTorque,FrontRightThighMaxTorque,BackLeftThighMaxTorque,BackRightThighMaxTorque]);
kneeMax = max([FrontLeftKneeMaxTorque,FrontRightKneeMaxTorque,BackLeftKneeMaxTorque,BackRightKneeMaxTorque]);

fprintf('******\n');
fprintf('Hip: %2.2f Nm, Thigh: %2.2f Nm, Knee: %2.2f Nm\n', hipMax, thighMax, kneeMax);
fprintf('******\n');