filename = 'ProcessedLegData.xlsx';
warning('off','MATLAB:xlswrite:AddSheet')

xlswrite(filename,frontLeftHip,'Front Left Leg','A1')
xlswrite(filename,frontLeftThigh,'Front Left Leg','B1')
xlswrite(filename,frontLeftKnee,'Front Left Leg','C1')

xlswrite(filename,frontRightHip,'Front Right Leg','A1')
xlswrite(filename,frontRightThigh,'Front Right Leg','B1')
xlswrite(filename,frontRightKnee,'Front Right Leg','C1')

xlswrite(filename,backLeftHip,'Back Left Leg','A1')
xlswrite(filename,backLeftThigh,'Back Left Leg','B1')
xlswrite(filename,backLeftKnee,'Back Left Leg','C1')

xlswrite(filename,backRightHip,'Back Right Leg','A1')
xlswrite(filename,backRightThigh,'Back Right Leg','B1')
xlswrite(filename,backRightKnee,'Back Right Leg','C1')